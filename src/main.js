import { createApp } from 'vue'
import App from './App.vue'

import ElementPlus from 'element-plus'
import './element/theme.scss'
// import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
// import Avue from '../assets/lib/avue.min.js'

import AvueFormDesign from '../packages/index'

const app = createApp(App)
app.config.warnHandler = () => null

// 配置全局变量
// app.provide('dragDisabled', false)

// const handleDragDisabled = (value) => {
//   app.provide('dragDisabled', value)
// }

// app.provide('handleDragDisabled', handleDragDisabled)

app.use(ElementPlus, { locale: zhCn })
  .use(Avue)
  .use(AvueFormDesign)

app.mount('#app')
