export default [
  {
    labelPosition: 'right',
    labelSuffix: '：',
    labelWidth: 120,
    gutter: 0,
    menuBtn: true,
    submitBtn: true,
    submitText: '提交',
    emptyBtn: true,
    emptyText: '清空',
    temporaryText: '暂存',
    temporaryBtn: true,
    menuPosition: 'center',
    detail: false,
    column: [
      {
        type: 'dynamic',
        label: '合同信息2',
        span: 24,
        display: true,
        labelPosition: 'top',
        children: {
          align: 'center',
          headerAlign: 'center',
          index: false,
          addBtn: true,
          delBtn: true,
          column: [
            {
              type: 'date',
              label: '日期',
              span: 24,
              display: true,
              format: 'YYYY-MM-DD',
              valueFormat: 'YYYY-MM-DD',
              prop: 'startTime',
              defaultTime: [
                '00:00:00',
                '12:59:59'
              ]
            },
            {
              type: 'input',
              label: '合同编号',
              span: 24,
              display: true,
              prop: 'contractNo'
            },
            {
              type: 'select',
              label: '合同管理人',
              dicData: [
                {
                  label: '老张',
                  value: '1'
                },
                {
                  label: '老李',
                  value: '2'
                },
                {
                  label: '老郑',
                  value: '3'
                }
              ],
              cascader: [],
              span: 24,
              display: true,
              props: {
                label: 'label',
                value: 'value',
                desc: 'desc'
              },
              prop: 'userId'
            },
            {
              type: 'input',
              label: '合同金额',
              span: 24,
              display: true,
              prop: 'totalMoney'
            }
          ]
        },
        prop: 'contractList',
        order: 2
      }
    ],
    tplName: '测试表单',
    readonly: false,
    disabled: false,
    group: [
      {
        label: '诉讼申请1',
        prop: 'apply',
        arrow: false,
        collapse: true,
        display: true,
        order: 1,
        column: [
          {
            type: 'input',
            label: '诉讼编号',
            span: 6,
            display: true,
            prop: 'applyNo',
            rules: [
              {
                required: true,
                message: '诉讼编号必须填写'
              },
              {
                pattern: '^[1]([3-9])[0-9]{9}$',
                message: '诉讼编号格式不匹配'
              }
            ],
            required: true,
            patternType: '^[1]([3-9])[0-9]{9}$',
            pattern: '^[1]([3-9])[0-9]{9}$'
          },
          {
            type: 'select',
            label: '公司主体',
            span: 6,
            display: true,
            props: {
              label: 'label',
              value: 'value',
              desc: 'desc'
            },
            dicData: [
              {
                label: '选项一',
                value: '0',
                desc: '选项一'
              },
              {
                label: '选项二',
                value: '1',
                desc: '选项二'
              },
              {
                label: '选项三',
                value: '2',
                desc: '选项三'
              }
            ],
            prop: 'firmId',
            rules: [
              {
                required: true,
                message: '公司主体必须填写'
              }
            ]
          },
          {
            type: 'input',
            label: '创建人',
            span: 6,
            display: true,
            prop: 'createName',
            rules: [
              {
                required: true,
                message: '创建人必须填写'
              }
            ]
          },
          {
            type: 'date',
            label: '创建时间',
            span: 6,
            display: true,
            format: 'YYYY-MM-DD',
            valueFormat: 'YYYY-MM-DD',
            prop: 'createTime',
            rules: [
              {
                required: true,
                message: '创建时间必须填写'
              }
            ]
          },
          {
            type: 'select',
            label: '接口数据字典',
            cascader: [],
            span: 6,
            display: true,
            props: {
              label: 'label',
              value: 'value',
              desc: 'desc'
            },
            prop: 'select_post',
            dicUrl: '/get/treeData',
            dicMethod: 'post',
            dicQuery: {
              firmId: '1'
            }
          },
          {
            type: 'select',
            label: '接口数据字典2',
            cascader: [],
            span: 6,
            display: true,
            props: {
              label: 'label',
              value: 'value',
              desc: 'desc'
            },
            prop: 'a164931251931697221',
            dicUrl: '/get/treeData',
            dicMethod: 'post',
            dicQuery: {
              firmId: '1',
              userId: '${}'
            }
          },
          {
            type: 'input',
            label: '隐藏域',
            span: 6,
            display: false,
            prop: 'id'
          }
        ]
      }
    ]
  }
]