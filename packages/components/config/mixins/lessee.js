export default {
  props: ['data', 'column', 'select', 'index'],
  emits: ['update:select', 'change'],
  data() {
    return {
      selectWidget: this.select
    }
  },
  mounted() {
    // console.log(this.selectWidget)
  },
  methods: {
    handleSelectWidget(index) {
      this.selectWidget = this.data.column[index]
    },
    handleWidgetDelete(index) {
      if (this.data.column.length - 1 === index) {
        if (index === 0) this.selectWidget = {}
        else this.handleSelectWidget(index - 1)
      } else this.handleSelectWidget(index + 1)

      this.$nextTick(() => {
        this.data.column.splice(index, 1)
        this.$emit("change")
      })
    }
  },
  watch: {
    select(val) {
      this.selectWidget = val
    },
    selectWidget: {
      handler(val) {
        this.$emit('update:select', val)
      },
      deep: true
    }
  }
}